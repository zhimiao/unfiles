package main

import (
	"crypto/sha256"
	"encoding/hex"
	"io"
	"os"
)

func GetHash(path string) (string, error) {
	file, err := os.Open(path)
	if err != nil {
		return "", err
	}
	defer file.Close()
	hob := sha256.New()
	_, err = io.Copy(hob, file)
	if err != nil {
		return "", err
	}
	hash := hob.Sum(nil)
	return hex.EncodeToString(hash), nil
}
