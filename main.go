package main

import (
	"fmt"
	"os"
	"sync"
	"time"
)

type safeMap struct {
	sync.RWMutex
	data map[string][]string
}

var s *safeMap
var lock int

func main() {
	s = new(safeMap)
	s.data = make(map[string][]string)
	root , err := os.Getwd()
	if err != nil {
		fmt.Println("当前目录获取失败", err.Error())
		return
	}
	fmt.Println("正在扫描", root)
	dir, err := WalkDir(root, nil)
	if err != nil {
		fmt.Println("文件扫描失败", err.Error())
		return
	}
	task := make(chan string)
	pool(task)
	fmt.Println("扫描完毕")
	for _, fpath := range dir {
		task <- fpath
	}
	for {
		time.Sleep(time.Second)
		if lock == len(dir) {
			break
		}
	}
	s.RLock()
	for k, vs := range s.data {
		fmt.Println(k, vs)
	}
	fmt.Println("建议删除")
	for _, vs := range s.data {
		if len(vs) > 1 {
			for i, v := range vs {
				if i != 0 {
					fmt.Println(v)
				}
			}
		}
	}
	fmt.Print("是否删除[yes/no]:")
	var scanStr string
	num, err := fmt.Scanln(&scanStr)
	if err != nil || num == 0 || scanStr != "yes" {
		fmt.Println("取消删除")
		return
	}
	for _, vs := range s.data {
		if len(vs) > 1 {
			for i, v := range vs {
				if i != 0 {
					os.Remove(v)
				}
			}
		}
	}
}

func pool(task <-chan string)  {
	for i := 0; i < 100; i++ {
		go func() {
			for {
				m := <-task
				hash, err := GetHash(m)
				if err != nil {
					continue
				}
				s.Lock()
				fpaths := make([]string, 0)
				if t, ok := s.data[hash]; ok {
					fpaths = t
				}
				s.data[hash] = append(fpaths, m)
				s.Unlock()
				lock++
				fmt.Println("end:", hash, m)
			}
		}()
	}
}
