package main

import (
	"fmt"
	"testing"
)

func TestWalkDir(t *testing.T) {
	dir, err := WalkDir("C:\\Users\\zhimiao\\Desktop\\frpddns", nil)
	if err != nil {
		fmt.Println(err)
	}
	for i, s := range dir {
		fmt.Printf("%d->%s\n", i, s)
	}
}
