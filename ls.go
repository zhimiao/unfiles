package main

import (
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
)

// 获取指定目录下的所有文件，不进入下一级目录搜索，可以匹配后缀过滤。
func ListDir(dirPth string, suffix string) (files []string, err error) {
	files = make([]string, 0, 10)
	dir, err := ioutil.ReadDir(dirPth)
	if err != nil {
		return nil, err
	}
	PthSep := string(os.PathSeparator)
	suffix = strings.ToUpper(suffix) // 忽略后缀匹配的大小写
	for _, fi := range dir {
		if fi.IsDir() { // 忽略目录
			continue
		}
		if strings.HasSuffix(strings.ToUpper(fi.Name()), suffix) { // 匹配文件
			files = append(files, dirPth+PthSep+fi.Name())
		}
	}
	return files, nil
}

// 获取指定目录及所有子目录下的所有文件，可以匹配后缀过滤。
func WalkDir(dirPth string, suffix []string) (files []string, err error) {
	files = make([]string, 0, 30)
	err = filepath.Walk(dirPth, func(filename string, fi os.FileInfo, err error) error { // 遍历目录
		// if err != nil { //忽略错误
		// return err
		// }
		if fi.IsDir() { // 忽略目录
			return nil
		}
		if len(suffix) == 0 || CheckSuffix(fi.Name(), suffix) {
			files = append(files, filename)
		}
		return nil
	})

	return files, err
}

func CheckSuffix(name string, suffix []string) bool {
	name = strings.ToUpper(name)
	for _, v := range suffix {
		v = strings.ToUpper(v)
		if strings.HasSuffix(name, v) {
			return true
		}
	}
	return false
}